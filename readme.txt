=== Blox Lite - Content Blocks for Genesis ===
Contributors: ndiego, outermost design
Donate Link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=52EAFXT8UV3XQ
Tags: genesis, content block, add content, genesis hooks, Genesis Framework, genesiswp, header, footer, sidebar, custom header, featured image
Requires at least: 3.8
Tested up to: 4.4.2
Stable tag: 1.0.2
License: GNU General Public License v2.0 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily add custom content to your Genesis powered website.

== Description ==

**Please Note:** This plugin is only for Genesis Framework users. Genesis is a premium product by [StudioPress](http://www.studiopress.com). We have no affiliation with the company. We just love building useful tools for Genesis users.

Blox Lite provides you with the easiest way to add custom content to your Genesis powered website. Lets assume you have picked out a theme for your website, but wish you could add a banner image to each post. Or perhaps you want to add a call-to-action just before the site footer but there is no designated widget area for that location. Now you could search the web for the needed code and modify your theme’s functions.php file, but there are 3 problems with this approach:

1. It is not easy!
1. Any updates by the theme's developer will overwrite your modifications
1. Your modifications will not be automatically transferred if you ever desire to switch themes

Blox Lite is the solution. Check out the video below for an introduction to this plugin. 

https://www.youtube.com/watch?v=uPAR-GI6sxQ
*Skip ahead to 4:40 and 9:10 for the examples*.

= Features =
* Add content locally to posts, pages, and all public custom post types
* Add content globally to virtually any page(s) on your website including 404 pages, search pages, and archive pages
* Add as many content blocks to a page as you want
* Position content anywhere there is a Genesis Hook
* Display a custom image or featured image
* Display HMTL content
* Add scripts, CSS, iframes, and even PHP
* Plugin is fully translatable (.pot file included)

= Content Options =
* Raw Content
* Static Image

You might be wondering why there are only two content options… Well this is the lite version of [Blox](https://www.bloxwp.com/?utm_source=blox-lite&utm_medium=plugin&utm_content=readme-links&utm_campaign=Blox_Plugin_Links), which is a premium plugin that will be available for purchase soon. Not all of the content options that are available in Blox are included in Blox Lite. But don't worry, arguably the two most important ones are! With Raw Content you can add anything to your website, whether that be raw HTML, CSS, Javascript, iframes, shortcodes, and even PHP. If you are looking to add images, such as a banner image, the Static Image option is the way to go. Add a photo directly from your media library, choose the size, add a caption, and more. The Static Image option even supports background images. This is great for setting up responsive headers or adding a parallax effect.

= What Blox Lite Doesn’t Do =

As a general rule, Blox Lite allows you to **add** content. It does not remove existing content. For example, say you hate the footer on your theme and you want to create a custom one using this plugin. Blox Lite will easily enable you to add the new footer, but the old one will be there as well. To remove content, you will need to add the necessary PHP code to your theme’s functions.php file. 

Blox Lite does not include any styling aside from a small amount of default CSS for images, but this can be turned off in the Settings. It is your site and we do not want to impose styling choices on you. Blox Lite will inherit most of your theme’s styling, but you will likely need to add some CSS to get things looking exactly how you want them. This customizability is an intended feature of Blox Lite and makes it all that more powerful. Simple questions about styling can be asked in the plugin’s support forum. If you would like in depth assistance, please look into purchasing the full version of [Blox](https://www.bloxwp.com/?utm_source=blox-lite&utm_medium=plugin&utm_content=readme-links&utm_campaign=Blox_Plugin_Links), which will be available soon. A paid Blox license comes with extensive plugin support.

= Support This Plugin = 

There are many ways you can support the development of this plugin:

1. If you spot an error or bug, please let us know in the support forums. The issue will be diagnosed an a new release push out as soon as possible.
1. If you are looking for additional features, we encourage you to check out the premium version of [Blox](https://www.bloxwp.com/?utm_source=blox-lite&utm_medium=plugin&utm_content=readme-links&utm_campaign=Blox_Plugin_Links). The more people that purchase Blox, the more likely additional features will work their way into both Blox and Blox Lite.
1. [Donate](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=52EAFXT8UV3XQ). Time is money, and contributions from users like you really help us dedicate more hours to the continual development and support of this plugin. 

== Installation ==

1. You have a couple options:
	* Go to Plugins->Add New and search for "Blox Lite”. Once found, click "Install".
	* Download the folder from Wordpress.org and zip the folder. Then upload via Plugins->Add New->Upload.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. From the ‘Plugins’ page, head directly to the plugin ‘Settings’ page to configure your global options.
4. Once the global settings have been set, navigate to "All Global Blocks" to create a new global block. To create local blocks navigate to a post, page, or custom post type, locate the ‘Local Content Blocks’ metabox and click the "Add Content Block" button. If you have any implementation questions, please post in the plugin support forum or check out this [Getting Started Video](https://www.youtube.com/watch?v=uPAR-GI6sxQ).

**Please Note:** This plugin is only for Genesis Framework users. Genesis is a premium product by [StudioPress](http://www.studiopress.com). If you do not have Genesis 2.0+ or Wordpress 3.6+, the plugin will not activate.

== Frequently Asked Questions ==

1. **What are the requirements for Blox Lite?**
Blox Lite is a WordPress plugin that is designed for users of the Genesis Framework, which is a product of [StudioPress](http://www.studiopress.com). Therefore, a Genesis theme needs to be active for Blox Lite to work, and the site must also must be running Genesis v2.0 or greater. *It is important to note that we do not have any direct affiliation with Studiopress.*

	In addition to Genesis requirements, Blox Lite requires at least WordPress 3.8 and a modern web browser. Blox is not compatible with the WordPress.com platform. You must be using a self-hosted version of WordPress to use this plugin on your site.
	
1. **Is Blox Lite multisite compatible?**
Yes, Blox Lite works flawlessly with WordPress MultiSite, both network activated and activated on individual sites.

1. **Is Blox translatable?**
Yup. Blox Lite has full translation and localization support via the blox textdomain and a .pot file is provided. All .mo and .po translation files should go into the languages folder in the base of the plugin.

1. **I need help getting started, what resources are available?**
There are a couple resources that will help you get started with Blox Lite:
	* Watch our [Getting Started Video](https://www.youtube.com/watch?v=uPAR-GI6sxQ).
	* Check out the [Blox Documentation](https://www.bloxwp.com/documentation/?utm_source=blox-lite&utm_medium=plugin&utm_content=readme-links&utm_campaign=Blox_Plugin_Links). **Note** that these docs are for the premium version of Blox Lite, but most of the information is interchangeable. 
	* When all else fails, send us a support request and we will do our best to get you straightened out. For in depth priority support, consider purchasing a [Blox license](https://www.bloxwp.com/pricing/?utm_source=blox-lite&utm_medium=plugin&utm_content=readme-links&utm_campaign=Blox_Plugin_Links). 

== Screenshots ==

1. The Global Blocks admin screen
2. An example Global Block featuring the Raw Content content type
3. An example Local Block featuring the Static Image content type
4. The Blox settings screen

== Changelog ==

= 1.0.2 =
* Fixed bug with Static Image caption stripping HTML tags (Thanks Nestor!)
* Fixed bug with Featured/Custom pulling wrong featured image on non-singular pages (Thanks Nestor!)
* Updated a few informational links in the plugin

= 1.0.1 =
* Fixed bug with PHP 5.3 (Thanks Tom!)
* Updated a few informational links in the plugin

= 1.0.0 =
* Initial Release